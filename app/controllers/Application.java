package controllers;

import models.User;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Controller;
import models.*;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import play.i18n.Lang;
import play.mvc.Before;
import play.mvc.Controller;

public class Application extends Controller {

    public static void index() {
        render();
    }

    public static void signInPage() {
        renderArgs.put("message", flash.get("error"));
        if (session.get("logged") == null) {
            renderArgs.put("headerContent", true);
            renderTemplate("Auth/SignInPage.html");
        } else {
            Profile.profile();
        }
    }

    public static void authenticate(@Required String phone,
                                    @Required String password) {
        checkAuthenticity();
        if (Validation.hasErrors()) {
            flash.put("error", "Укажите логин и пароль правильно");
            signInPage();
        }
        User user = User.find("phone = ?1", phone).first();
        if (user == null) {
            flash.put("error", "Неправильный логин, проверьте ваш логин и попробуйте еще раз");
            signInPage();
        }
        if (!password.equals(user.password)) {
            flash.put("error", "Неправильный пароль");
            signInPage();
        }

        session.put("logged", phone);

        Profile.profile();
    }

    public static void authRegister(@Required String firstname,
                                    @Required String lastname,
                                    @Required String phone,
                                    @Required String password){
        if(Validation.hasErrors()){
            signUpPage("Fill all the fields");
        }
        User exAccount = User.find("phone=?1", phone).first();
        if(exAccount != null){
            signUpPage("Already registered login");
        }

        User newAccount = new User();
        newAccount.firstname = firstname;
        newAccount.lastname = lastname;
        newAccount.password = password;
        newAccount.phone = phone;

        newAccount.save();

        session.put("logged", phone);

        Profile.profile();
    }

    public static void signUpPage(String message) {
        renderArgs.put("message", flash.get("error"));
        renderArgs.put("headerContent", true);
        renderTemplate("Auth/SignUpPage.html");
    }

    public static void logout() {
        session.remove("logged");
        session.clear();
        signInPage();
    }
}