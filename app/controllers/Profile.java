package controllers;

import models.User;
import play.Play;
import play.mvc.Controller;

public class Profile extends Controller {
    private static final String privateDirectory = Play.configuration.getProperty("zhastar.private.folder.path");
    private static final String path_check = "payment_check";

    public static void profile() {
        if (session.get("logged") == null) {
            Application.signInPage();
        }
        User user = User.find("phone = ?1", session.get("logged")).first();
        renderArgs.put("headerContent", true);
        renderArgs.put("username", session.get("logged"));
        render(user);
    }



}
