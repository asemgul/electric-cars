package controllers;

import models.User;
import models.Station;
import play.Play;
import play.mvc.Controller;

public class Station extends Controller {
    /**
     * Renders the list of all stations.
     */
    public static void getAllStations() {
        List<Station> stationList = Station.findAll();
        render(stationList);
    }
}
