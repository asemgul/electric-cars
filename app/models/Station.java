package models;

import play.Logger;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by asemgul on 4/28/22.
 */
@Entity
@Table(name = "stations")
public class Station extends GenericModel {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "city")
    public String city;

    @Column(name = "latitude")
    public float latitude;

    @Column(name = "longitude")
    public float longitude;

    @Column(name = "address")
    public String address;

    @Column(name = "description")
    public String description;

    @Column(name = "image_url")
    public String imageUrl;

    @Column(name = "created_at")
    public Date createdAt;

    @Column(name = "updated_at")
    public Date updatedAt;
}
