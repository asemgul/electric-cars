package models;

import play.Logger;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by asemgul on 5/17/17.
 */
@Entity
@Table(name = "users")
public class User extends GenericModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "firstname")
    public String firstname;

    @Column(name = "lastname")
    public String lastname;

    @Column(name = "phone")
    public String phone;

    @Column(name = "password")
    public String password;

    @Column(name = "operation_info")
    public Integer operationInfo;

    @Column(name = "money")
    public float money;

    @Column(name = "power_need")
    public float powerNeed;

    @Column(name = "ecs_id")
    public Integer ecsId;

    @Column(name = "created_at")
    public Date createdAt;

    @Column(name = "updated_at")
    public Date updatedAt;
}
