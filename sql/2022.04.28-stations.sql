DROP TABLE IF EXISTS stations;
CREATE TABLE stations (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    city varchar(255) NOT NULL,
    latitude float NOT NULL,
    longitude float NOT NULL,
    address varchar(255) NOT NULL,
    description varchar(255) DEFAULT NULL,
    image_url varchar(255) DEFAULT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.224665, 76.909020, '​Тимирязева, 42/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.232513, 76.950451, 'пр-т. Назарбаева, 42/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.234771, 76.936341, 'пр-т. Сейфуллина, 617/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.272022, 76.931296, 'Коммунальная, 8');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.207905, 76.970359, 'Проспект Достык, 341');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.299029, 77.004037, '​Кульджинский тракт, 2/1');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.206360, 76.897764, '​​Радостовца, 292/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.262362, 76.983600, '​​​Сағадат Нұрмағамбетов, 520/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.207593, 76.858685, '​Жандосова, 83/1');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.226590, 76.863458, '​ТРЦ MOSKVA Metropolitan​8-й микрорайон, 37/1');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.233561, 76.956782, '​​ТРЦ DOSTYK PLAZA​микрорайон Самал-2, 111');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.264141, 76.900044, '​​проспект Райымбека, 239г/4');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.299022, 77.003997, '​​Кульджинский тракт, 2/1');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.202993, 76.892511, '​​ТРЦ Mega Center Alma-Ata​Розыбакиева, 247а');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.297161, 76.949704, '​​проспект Суюнбая, 159а');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.244464, 76.835761, '​​​проспект Райымбека, 514а/2');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.211472, 76.842888, '​​​ТРЦ SPUTNIK mall​микрорайон Мамыр 1, 8а');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.129254, 77.083444, '​​​Керей-Жәнібек хандар, 640/5');

INSERT INTO stations (city, latitude, longitude, address)
VALUES ('Almaty', 43.346528, 77.012015, '​​​Майлина, 2/5');